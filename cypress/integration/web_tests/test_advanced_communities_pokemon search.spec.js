/// <reference types="cypress" />


Cypress.on('uncaught:exception', (err) => {
  if(err != null)
    cy.log(`Uncritical error is found - ${err}`)
  return false
})

describe('Advanced communities Pokémon search', () => {
  it('Visit pokemon community wiki', () => {
    cy.visit('https://bulbapedia.bulbagarden.net/wiki/Main_Page');
    cy.url()
      .should('include', '.net/wiki/Main_Page');
    cy.get('tbody>tr>th').contains('Welcome to Bulbapedia!');
  });

  it('Click on search input field do search', () =>{
    const searchText = 'communities';
    const searchField = cy.get('#searchform');
    searchField
      .click()
      .type(searchText);

    const searchInput = searchField.children('#searchInput');
    if (searchInput.should('have.value', searchText))
      searchField.focus().type('{enter}');
  });

  const checkboxName = 'ns11'
  const checkboxValue = '1'

  it('Click on advanced button and select "Widget" checkbox', () => {
    const advancedBtn = cy.get('.search-types > ul > li > a').contains('Advanced');
    advancedBtn.click();
    const seacrhForm = cy.get('#powersearch')
    const widgetCheckbox = seacrhForm
      .find(`input[name=${checkboxName}]`)
      .should('not.be.checked')
      .scrollIntoView()
      .check()
  });

  it('Make advanced search with checkbox and check url contains needed options', () => {
    const seacrhForm = cy.get('#powersearch')
    seacrhForm.find('button').click()
    cy.url()
      .should('include', `${checkboxName}=${checkboxValue}`);
  });

})