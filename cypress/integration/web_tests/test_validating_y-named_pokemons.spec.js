/// <reference types="cypress" />


Cypress.on('uncaught:exception', (err) => {
  if(err != null)
    cy.log(`Uncritical error is found - ${err}`)
  return false
})

describe('Validating Y-named pokemons', () => {
  it('Visit list pokemons by names', () => {
    cy.visit('https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_name#Y');
    cy.url()
      .should('include', 'Pok%C3%A9mon_by_name#Y');
    cy.get('#firstHeading').contains('List of Pokémon by name');
  });

  it('Verify Yamask pokemon exists in present list', () =>{
    let tableYNames = cy.get('#Y')
      .parent()
      .nextUntil('table')
      .next()
      .find('tbody>tr')
      .contains('Yamask')
  });

  it("count the number of Pokémon's whose name start with the letter “Y”", () =>{
    let tableLength = cy.get('#Y')
      .parent()
      .nextUntil('table')
      .next()
      .find('tbody')
      .children()
      .then(($el) => {
        const count = $el.length - 1
        cy.log(count)
      })
  });
})